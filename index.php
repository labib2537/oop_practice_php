<!-- class, object, property, method -->

<?php

echo "Operators: <br>";

class Calculator
{
    public $n1 = 5;
    public $n2 = 8;
    function sum()
    {
        return 'Sum = ' . $this->n1+$this->n2;
    }
    function sub()
    {
        return 'Sub = ' . $this->n1-$this->n2;
    }
    function mul()
    {
        return 'Mul = ' . $this->n1*$this->n2;
    }
    function div(){
        return 'div = ' . $this->n1 / $this->n2;
    }
}

$obj = new Calculator;

// set new value or mutated value
echo 'n1 = ';
echo $obj->n1 = 75;
echo '<br>';
echo 'n2 = ';
echo $obj->n2 = 48;
echo '<br>';
echo $obj->sum();
echo '<br>';
echo $obj->sub();
echo '<br>';
echo $obj->mul();
echo '<br>';
echo $obj->div();

echo '<hr>';
echo "Circle Formulas: <br>";

class Circle{
    public $r=0;
    public $pi=3.1416;
    function dia()
    {
        return 'Diameter = '. 2 * $this->r;
    }
    function circum()
    {
        return 'Circumference = '. 2 * $this->pi * $this->r;
        
    }
    function area()
    {
        return 'Area = '. $this->pi * pow($this->r,2);     
    }
}
$obj_cir = new Circle();
echo 'r = ';

echo $obj_cir->r=4;
echo '<br>';

echo $obj_cir->dia();
echo '<br>';
echo $obj_cir->circum();
echo '<br>';
echo $obj_cir->area();
echo '<br>';

echo '<hr>';

echo "Math Formulas: <br>";

class Lzebra{
    public $a=0;
    public $b=0;
    function aplusb()
    {
        return '(a+b)^2 = '. $this->a*$this->a + 2 * $this->a * $this->b + $this->b*$this->b;
    }
    function aminusb()
    {
        return '(a-b)^2 = '. $this->a*$this->a - 2 * $this->a * $this->b + $this->b*$this->b;
        
    }
    
}
$obj_l = new Lzebra();
echo 'a = ';
echo $obj_l->a=3;
echo '<br>';

echo 'b = ';
echo $obj_l->b=2;
echo '<br>';

echo $obj_l->aplusb();
echo '<br>';
echo $obj_l->aminusb();
echo '<br>';



echo '<hr>';


echo "Calculation using calling one function into other functions: <br>";

class Calculator1
{
    public $n1 = 0;
    public $n2 = 0;
    function sum()
    {
        return 'Sum = ' . $this->n1+$this->n2;
    }
    function sub()
    {
        return 'Sub = ' . $this->n1-$this->n2;
    }
    function setNumber1($n)
    {
        $this->n1 = $n;
        return $this->n1;
    }
    function setNumber2($n)
    {
        $this->n2 = $n;
        return $this->n2;
    }
    function result(){
        return $this->sum() .'<br>'. $this->sub().'<br>';
    }
}

$obj1 = new Calculator1;

// set new value or mutated value
echo 'n1 = ';
echo $obj1->setNumber1(175);
echo '<br>';
echo 'n2 = ';
echo $obj1->setNumber2(125);
echo '<br>';
echo $obj1->sum();
echo '<br>';
echo $obj1->sub();
echo '<br>';
echo 'Result: <br>' . $obj1->result();
echo '<br>';


echo '<hr>';

// Home Work
/*
take user input using form and make calculation
*/

?>
<!-- calculator updated -->

<center><h3><?php echo "Calculator Updated"; ?></h3></center>

<?php



class RealCalculator {
  private $num1;
  private $num2;

  public function __construct($num1, $num2) {
    $this->num1 = $num1;
    $this->num2 = $num2;
  }

  public function add() {
    return $this->num1 + $this->num2;
  }

  public function sub() {
    return $this->num1 - $this->num2;
  }

  public function mul() {
    return $this->num1 * $this->num2;
  }

  public function div() {
    if ($this->num2 == 0) {
      return "Math ERROR!";
    } else {
      return $this->num1 / $this->num2;
    }
  }
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  $num1 = $_POST['n1'];
  $num2 = $_POST['n2'];
  $operator = $_POST['op'];

  $calculator = new RealCalculator($num1, $num2);

  switch ($operator) {
    case 'add':
      $result = $calculator->add();
      break;
    case 'subtract':
      $result = $calculator->sub();
      break;
    case 'multiply':
      $result = $calculator->mul();
      break;
    case 'divide':
      $result = $calculator->div();
      break;
    default:
      $result = "Invalid operator!";
      break;
  }
}

?>

<!DOCTYPE html>
<html>
<head>
  <title>OOP Practice in PHP</title>

  <style>
    .my-calculator{
        border: 1px solid black;
        width: 450px;
        height: 460px;
        background: #CB8AFF;
        text-align: center;
        position: relative;
        left: 35%;
        border-radius: 15px;
        border: none;
        
    }
    button{
        padding-left: 20px;
        padding-right: 20px;
        padding-top: 16px;
        padding-bottom: 16px;
        border-radius: 10px;
        font-size: 20px;
        background: #B75CFF;
        border: none;
        margin: 10px;
        cursor: pointer;
        color: #5C00A3;

    }
    h2{
       padding: 12px;
       color: #7500D1;
    }
    button:hover{
        transform: scale(1.3);
    }
    input{
        padding: 15px;
        font-size: 18px;
        border: none;
        margin-left: 10px;
        border-radius: 10px;
        color: #420075;
    }

    label{
        font-size: 20px;
        color: #5C00A3;
    }
  </style>  

  <script>
    function clearResult() {
      document.getElementById("result").value = "<?=$result?>";
    }
  </script>

</head>

<body onload="clearResult()">

  <div class="my-calculator">
  <h2>Calculator</h2>
  <form method="post" action="<?=$_SERVER["PHP_SELF"]?>">
    <label for="number1" class="text">Enter Number 1:</label>
    <input type="number" name="n1" id="n1" value="<?php echo $num1; ?>" required>
    <br><br>

    <label for="number2" class="text">Enter Number 2:</label>
    <input type="number" name="n2" id="n2" value="<?php echo $num2; ?>" required>
    <br><br>

    <button type="submit" name="op" value="add">+</button>
    <button type="submit" name="op" value="subtract">-</button>
    <button type="submit" name="op" value="multiply">X</button>
    <button type="submit" name="op" value="divide">/</button>
    <br><br>

    <label for="result" class="text">Result:</label>
    <input type="text" name="result" id="result" value="<?php echo $result; ?>" readonly>
    <br><br>

    <button type="button" onclick="document.getElementById('n1').value=''; document.getElementById('n2').value=''; document.getElementById('result').value='';">Clear</button>
  </form>
  </div>
  

</body>
</html>




<?php

echo '<hr>';

class Student
{
  public $name;
  public $address;
  public $mail;
  public $phone;

  public function setName($name)
  {
    $this->name = $name;
    return $this;
  }

  public function setAddress($address)
  {
    $this->address = $address;
    return $this;
  }

  public function setMail($mail)
  {
    $this->mail = $mail;
    return $this;
  }

  public function setPhone($phone)
  {
    $this->phone = $phone;
    return $this;
  }

  public function getStudent()
  {
    $studentInfo = [
       'name' => $this->name,
       'address' => $this->address,
       'email' => $this->mail,
       'phone' => $this->phone
    ];
    return $studentInfo;
  }
}

$obj_std = new Student;
$obj_std->setName('Farhan Labib');
$obj_std->setAddress('Tongi Bazar, Gazipur');
$obj_std->setMail('sk.karib35@gmail.com');
$obj_std->setPhone('01712988806');
echo '<pre>';
var_dump($obj_std -> getStudent()); 

$studentInfo = $obj_std->getStudent();

print_r($studentInfo);


echo '<hr>';
echo "Inheritence <br><br>";

Class A{
  public $p1 = 'Public Property';
  private $p2 = 'Private Property';
  protected $p3 = 'protected Property';

  public function privateProperty(){
    return $this->p2;
  }

}


$objA = new A;

echo $objA->p1;
echo '<br>';
echo $objA->privateProperty(); // so, we can access private property using public method

echo '<hr>';
echo "Single Inheritence <br><br>";

Class AA{
  public $p1 = 'Public Property';
  private $p2 = 'Private Property';
  protected $p3 = 'protected Property';

  public function privateProperty(){
    return $this->p2;
  }

}

class B extends AA{
  public function accessProtectedProperty(){
    return $this->p3;
  }
}

$obj_B = new B;
echo $obj_B->p1;
echo '<br>';
echo $obj_B->accessProtectedProperty();


echo '<hr>';
echo "Multiple Inheritence <br><br>";

Trait DB{
  public function connect($name)
  {
    return $name .' is Connected to our DataBase<br>';
  }
}

Trait Notification{
  public function sent($name, $message)
  {
    return 'Hello! '. $name .' '. $message ; 
  }
}

Trait Time{
  public function timeDate($tm)
  {
    return 'Your class will start '.$tm;
  }
}

Trait Greet{
  public function greetings($name)
  {
    return 'Best of Luck! '.$name . '<br><br><br.';
  }
}

class Students{

  public $name = 'Farhan Labib';
  public $tm = 'from 1st June at 9 AM<br>';

}

class Employee extends Students{
  use DB, Notification, Time, Greet;
}

$emp = new Employee;

echo $emp->sent($emp->name,'Welcome You are Selected in Php with Laravel Framework Course<br>');
echo $emp->connect($emp->name);
echo $emp->timeDate($emp->tm);
echo $emp->greetings($emp->name);



