<?php

class RealCalculator {
  private $num1;
  private $num2;

  public function __construct($num1, $num2) {
    $this->num1 = $num1;
    $this->num2 = $num2;
  }

  public function add() {
    return $this->num1 + $this->num2;
  }

  public function sub() {
    return $this->num1 - $this->num2;
  }

  public function mul() {
    return $this->num1 * $this->num2;
  }

  public function div() {
    if ($this->num2 == 0) {
      return "Math ERROR!";
    } else {
      return $this->num1 / $this->num2;
    }
  }
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  $num1 = $_POST['n1'];
  $num2 = $_POST['n2'];
  $operator = $_POST['op'];

  $calculator = new RealCalculator($num1, $num2);

  switch ($operator) {
    case 'add':
      $result = $calculator->add();
      break;
    case 'subtract':
      $result = $calculator->sub();
      break;
    case 'multiply':
      $result = $calculator->mul();
      break;
    case 'divide':
      $result = $calculator->div();
      break;
    default:
      $result = "Invalid operator!";
      break;
  }
}

?>

<!DOCTYPE html>
<html>
<head>
  <title>OOP Practice in PHP</title>

  <style>
    .my-calculator{
        border: 1px solid black;
        width: 400px;
        height: 350px;
        background: #CB8AFF;
        text-align: center;
        position: relative;
        left: 40%;
        
    }
    button{
        padding: 8px;
        font-size: 15px;
        background: #B75CFF;
    }
    input{
        padding: 8px;
        font-size: 15px;
    }
  </style>  

  <script>
    function clearResult() {
      document.getElementById("result").value = "<?=$result?>";
    }
  </script>

</head>

<body onload="clearResult()">

  <div class="my-calculator">
  <h2>Calculator</h2>
  <form method="post" action="<?=$_SERVER["PHP_SELF"]?>">
    <label for="number1">Enter Number 1:</label>
    <input type="number" name="n1" id="n1" value="<?php echo $num1; ?>" required>
    <br><br>

    <label for="number2">Enter Number 2:</label>
    <input type="number" name="n2" id="n2" value="<?php echo $num2; ?>" required>
    <br><br>

    <button type="submit" name="op" value="add">+</button>
    <button type="submit" name="op" value="subtract">-</button>
    <button type="submit" name="op" value="multiply">X</button>
    <button type="submit" name="op" value="divide">/</button>
    <br><br>

    <label for="result">Result:</label>
    <input type="text" name="result" id="result" value="<?php echo $result; ?>" readonly>
    <br><br>

    <button type="button" onclick="document.getElementById('n1').value=''; document.getElementById('n2').value=''; document.getElementById('result').value='';">Clear</button>
  </form>
  </div>
  

</body>
</html>

